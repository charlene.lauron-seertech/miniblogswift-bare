//
//  AddBlogController.swift
//  MiniBlog
//
//  Created by Allister Alambra on 10/6/15.
//  Copyright © 2015 Seer Technologies, Inc. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class AddBlogController: BaseController{
    
    var addBlogView: AddBlogView?
    var selectedID: Int = -1
    var selectedDate: NSDate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load Targeted Xibs
        self.loadXibName("AddBlogView")
        self.addBlogView = (self.view as! AddBlogView)
        
        // Initialize UI Elements
        self.initializeNavigationBar()

        if self.selectedDate != nil {
            self.retrieveData()
        }
    }
    
    func retrieveData() {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        if(self.selectedDate != nil) {
            let predicate = NSPredicate(format: "date == %@", self.selectedDate!)
            
            let fetchRequest = NSFetchRequest(entityName: "BlogPost")
            fetchRequest.predicate = predicate
            
            do {
                let fetchResults = try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest) as? [BlogPost]
                if fetchResults!.count != 0{
                    let managedObject = fetchResults![0]
                    self.addBlogView?.blogTitleTextField.text = String(managedObject.valueForKey("title")!)
                    self.addBlogView?.blogPostTextView.text = String(managedObject.valueForKey("content")!)
                }
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
    }
    
    func updatePost() {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        if(self.selectedDate != nil) {
            let predicate = NSPredicate(format: "date == %@", self.selectedDate!)
            
            let fetchRequest = NSFetchRequest(entityName: "BlogPost")
            fetchRequest.predicate = predicate
            
            do {
                let fetchResults = try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest) as? [BlogPost]
                    if fetchResults!.count != 0{
                        let managedObject = fetchResults![0]
                        managedObject.setValue(self.addBlogView?.blogTitleTextField.text, forKey:"title")
                        managedObject.setValue(self.addBlogView?.blogPostTextView.text, forKey:"content")
                        managedObject.setValue(NSDate(), forKey:"date")
                        
                        try managedContext.save()
                    }
                self.navigationController?.popViewControllerAnimated(true)
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
    }
    
    // MARK: CoreData Methods
    func postBlog(){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let entity =  NSEntityDescription.entityForName("BlogPost",
            inManagedObjectContext:managedContext)
        let blogPost:BlogPost = BlogPost(entity: entity!,
            insertIntoManagedObjectContext: managedContext)
        
        blogPost.title = self.addBlogView?.blogTitleTextField.text
        blogPost.content = self.addBlogView?.blogPostTextView.text
        blogPost.date = NSDate()
        
        let authEntity =  NSEntityDescription.entityForName("Author",
            inManagedObjectContext:managedContext)
        let auth:Author = Author(entity: authEntity!,
            insertIntoManagedObjectContext: managedContext)
        print("teeeeest", GoogleSession.sharedInstance.googleName)
        auth.name = readFromPlist("Default Author")
        if GoogleSession.sharedInstance.googleName != "" {
            auth.name = GoogleSession.sharedInstance.googleName
        }
        
        blogPost.blogpost_author = auth
        
        do {
            try managedContext.save()
            self.navigationController?.popViewControllerAnimated(true)
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    // MARK: Controller Initializers
    
    func initializeNavigationBar(){
        var addBlogButtonItem : UIBarButtonItem!
        if self.selectedDate == nil {
            addBlogButtonItem = UIBarButtonItem(title: "Post", style: .Plain, target: self, action: Selector("postBlog"))
        } else {
            addBlogButtonItem = UIBarButtonItem(title: "Update", style: .Plain, target: self, action: Selector("updatePost"))
        }
        
        self.navigationItem.rightBarButtonItem = addBlogButtonItem
        
    }
    
}