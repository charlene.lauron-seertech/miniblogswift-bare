//
//  LoginViewController.swift
//  MiniBlog
//
//  Created by Charlene Lauron on 10/6/15.
//  Copyright © 2015 Seer Technologies, Inc. All rights reserved.
//

import Foundation
import UIKit

class LoginViewController : BaseController, GIDSignInUIDelegate, GIDSignInDelegate, LoginViewDelegate
{
    var loginView: LoginView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signInSilently()
        // Load Targeted Xibs
        self.loadXibName("LoginView")
        self.loginView = (self.view as! LoginView)
        
        self.loginView?.delegate = self;
        self.loginView?.loading.hidden = true;
    }
    
    func loginButtonPressed() {
        GIDSignIn.sharedInstance().signIn()
        self.loginView?.loading.hidden = false
        self.loginView?.loginButton.enabled = false
    }
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!, withError error: NSError!) {
            if (error == nil) {
                GoogleSession.sharedInstance.googleName = user.profile.name
                self.performSegueWithIdentifier("goToLandingPage", sender: self)
            } else {
                print("\(error.localizedDescription)")
            }
    }
    
    // pressed the Sign In button
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        self.loginView?.loading.hidden = true;
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!, presentViewController viewController: UIViewController!) {
        self.presentViewController(viewController, animated: true, completion: nil)
        self.loginView?.loading.hidden = true;
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!, dismissViewController viewController: UIViewController!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}