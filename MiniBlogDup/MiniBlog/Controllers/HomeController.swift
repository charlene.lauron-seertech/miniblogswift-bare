//
//  ViewController.swift
//  MiniBlog
//
//  Created by Allister Alambra on 10/6/15.
//  Copyright © 2015 Seer Technologies, Inc. All rights reserved.
//

import UIKit
import CoreData

class HomeController: BaseController, UITableViewDataSource, UITableViewDelegate, GIDSignInUIDelegate {

    var homeView: HomeView?
    var blogPosts: [BlogPost]?
    var selectedIndex: Int = -1
    var selectedDate: NSDate?
    
    // MARK: Lifecycle method override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load Targeted Xibs
        self.loadXibName("HomeView")
        self.homeView = (self.view as! HomeView)
        self.homeView?.blogTable.dataSource = self
        self.homeView?.blogTable.delegate = self
        
        // Initialize UI Elements
        self.initializeNavigationBar()
        
        // Register Custom TableView Class
        let nibName = UINib(nibName: "BlogPostCell", bundle:nil)
        self.homeView?.blogTable.registerNib(nibName, forCellReuseIdentifier: "BlogPostCell")
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.selectedIndex = -1
        self.selectedDate = nil
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "BlogPost")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            self.blogPosts = results as? [BlogPost]
            self.homeView?.blogTable.reloadData()
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    // MARK: Segue to AddBlogView
    func addBlogPost(){
        self.performSegueWithIdentifier("HomeToAddBlogSegue", sender: self)
    }
    
    func googleSignOut() {
        GIDSignIn.sharedInstance().signOut()
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let loginViewController = storyBoard.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
        self.presentViewController(loginViewController, animated:true, completion:nil)
        GoogleSession.sharedInstance.googleName = ""
    }
    
    // MARK: Controller Initializers
    
    func initializeNavigationBar(){
        
        let addBlogButtonItem = UIBarButtonItem(title: "Blog Now!", style: .Plain, target: self, action: Selector("addBlogPost"))
        
        let signOutButton : UIBarButtonItem = UIBarButtonItem(title: "Sign Out", style: UIBarButtonItemStyle.Plain, target: self, action: "googleSignOut")

        self.navigationItem.rightBarButtonItem = addBlogButtonItem
        self.navigationItem.leftBarButtonItem = signOutButton
    }
    
    // MARK: UITableViewDataSource Methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.blogPosts!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell = self.homeView?.blogTable.dequeueReusableCellWithIdentifier("BlogPostCell", forIndexPath: indexPath) as! BlogPostCell
        
        let blogPost:BlogPost = self.blogPosts![indexPath.row]
        
        cell.blogTitleLabel.text = blogPost.title! + " by " + blogPost.blogpost_author!.name!
        cell.blogPreviewLabel.text = blogPost.content!
        
        let dateFormatter = NSDateFormatter();
        dateFormatter.dateFormat = "MMM d h:mm a"
        
        cell.blogDateLabel.text = String(dateFormatter.stringFromDate(blogPost.date!))
        
        if(blogPost.blogpost_author!.name! != GoogleSession.sharedInstance.googleName) {
            cell.userInteractionEnabled = false
        }
        
        return cell
    }
    
    // MARK: UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        return 70.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedIndex = indexPath.row
        self.selectedDate = self.blogPosts![indexPath.row].date
        self.performSegueWithIdentifier("HomeToAddBlogSegue", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.destinationViewController.isKindOfClass(AddBlogController)) {
            let dest:AddBlogController = segue.destinationViewController as! AddBlogController
            dest.selectedID = self.selectedIndex
            dest.selectedDate = self.selectedDate
        }
    }
    
}

