//
//  LoginView.swift
//  MiniBlog
//
//  Created by Charlene Lauron on 10/6/15.
//  Copyright © 2015 Seer Technologies, Inc. All rights reserved.
//

import Foundation
import UIKit

protocol LoginViewDelegate {
    func loginButtonPressed()
}

class LoginView : BaseView, GIDSignInUIDelegate {

    var delegate: LoginViewDelegate?
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    @IBAction func loginButtonPressed(sender: AnyObject) {
        if self.delegate != nil && self.delegate?.loginButtonPressed != nil{
            self.delegate?.loginButtonPressed()
        }
    }
}