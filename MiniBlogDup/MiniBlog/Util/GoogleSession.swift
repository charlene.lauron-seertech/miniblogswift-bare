//
//  GoogleSession.swift
//  MiniBlog
//
//  Created by Charlene Lauron on 10/6/15.
//  Copyright © 2015 Seer Technologies, Inc. All rights reserved.
//

import Foundation

class GoogleSession {
    static let sharedInstance = GoogleSession()
    
    var googleName : String = ""
}